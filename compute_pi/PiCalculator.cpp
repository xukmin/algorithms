// Computes Pi using the Machin formula.
// 
// Created By: Min Xu <mxu@scu.edu> or <xukmin@gmail.com>
// Date: 11/26/2014

#include <ctime>
#include <iomanip>
#include <iostream>
#include <cmath>

#include <gmpxx.h>

static const int M = 10;

int DecimalToBit(int decimal_digits) {
  return decimal_digits * log2(10);
}

// Returns Arctan(1 / x)
mpf_class ArctanReciprocal(int x, int digits) {
  // initialize the accurate x
  // if the absolute value of a term is less than 10^-(digits+ M),
  // we will stop computing terms after it.
  mpf_class epsilon;
  mpf_class base = 10;
  mpf_pow_ui(epsilon.get_mpf_t(), base.get_mpf_t(), digits + M);
  epsilon = 1 / epsilon;

  // define varibles used in while loop
  int numerator = -1;
  mpf_class para(x);
  mpf_class result(0);
  int k = 1;
  int compare = 1;

  while (compare > 0) {
    numerator *= -1;

    mpf_class term = numerator / (k * para);
    result += term;  
 
    k += 2;
    para = para * x * x;
    compare = cmp(abs(term), epsilon);
  }

  return result;
}

mpf_class CalculatePi(int digits) {
  mpf_set_default_prec(DecimalToBit(digits + M));
  
  mpf_class pi = 4 * (4 * ArctanReciprocal(5, digits) -
                      ArctanReciprocal(239, digits));

  return pi;
}

void CalculateTime() {
  int digits = 100;
  double time = 0;

  while (time < 10) {
    std::clock_t start = std::clock();
    mpf_class pi = CalculatePi(digits);
    std::clock_t end = std::clock();

    time = (end - start) / double(CLOCKS_PER_SEC);
    std::cout << digits << ", "
              << std::fixed << std::showpoint << std::setprecision(6)
              << time << std::endl;
    
    digits = int(digits * 1.1);
  }
}

int main(int argc, char** argv) {
  if (argc == 1) {
    CalculateTime();
  } else if (argc == 2) {
    int digits = std::atoi(argv[1]);
    mpf_class pi = CalculatePi(digits);

    std::cout << std::fixed << std::setprecision(digits - 1)
              << pi << std::endl;
  } else {
    std::cout << "Usage: " << argv[0]
              << "[<decimal-digits-of-precision>]" << std::endl;
    return false;
  }

  return true;
}
