set terminal pdf
set output "PiRunTimes.pdf"
set xlabel "Digits of Precision"
set ylabel "Execution Time (s)"
set key top left
plot "PiRunTimes.txt" using 1:2 with linespoints title "Calculating Pi"

