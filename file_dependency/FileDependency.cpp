// This program is to implement the dependency logic of the make command line
// tool.
//
// The format of a makefile is:
//   target : dependency [dependency...]
// 
// The algorithm is to add a file name into a topological sorted list when its
// in-degree is zero, and delete all the edges from the file in the graph.
//
// Created By: Min Xu <mxu@scu.edu> or <xukmin@gmail.com>
// Date: 11/23/2014

#include <algorithm>
#include <cassert>
#include <iostream>
#include <fstream>
#include <map>
#include <list>
#include <set>
#include <string>
#include <vector>

struct Vertex {
  std::string name;
  bool is_target;  // whether we should build or check this vertex.
  std::vector<int> neighbors;
};

void PrintSortedList(const std::vector<int>& a,
                     const std::vector<Vertex>& graph) {
  for (int i = 0; i < a.size(); i++) {
    std::cout << graph[a[i]].name << " ";
    if (graph[a[i]].is_target) {
      std::cout << "build";
    } else {
      std::cout << "check";
    }
    std::cout << std::endl;
  }
}

// Separates a string using a splitter.
// Returns all separated parts in a vector. Note that empty parts will be
// ignored and won't be included in the vector.
std::vector<std::string> SplitString(std::string source, char splitter) {
  std::vector<std::string> str;
  std::size_t start_index = 0;
  std::size_t found = source.find(splitter);

  while (found != std::string::npos) {
    std::size_t length = found - start_index;
    if (length != 0) {
      str.push_back(source.substr(start_index, length));
    }
    start_index = found + 1;
    found = source.find(splitter, found + 1);
  }

  if (start_index != source.size()) {
    str.push_back(source.substr(start_index));
  }

  return str;
}

// Adds a file name (i.e. vertex) into both vertex_map and graph.
// Returns index of the added vertex in the graph.
int AddVertex(const std::string& name,
              std::map<std::string, int>& vertex_map,
              std::vector<Vertex>& graph) {
  int vertex_id;
  if (vertex_map.find(name) == vertex_map.end()) {
    vertex_id = vertex_map.size();
    vertex_map[name] = vertex_id;
    Vertex v;
    v.name = name;
    v.is_target = false;
    graph.push_back(v);
  } else {
    vertex_id = vertex_map[name];
  }
  return vertex_id;
}

// Reads input file and builds a graph.
// Each file is a vertex in the graph.
// All the dependencies of a file is kept as its neighbors. 
// Note that neighbors of a vertex in the graph are its incoming vertices.
void BuildGraphFromFile(char* fileName, std::vector<Vertex>& graph) {
  std::map<std::string, int> vertex_map;

  std::ifstream myfile(fileName);
  std::string aline;

  std::getline(myfile, aline);
  while (!myfile.eof()) {
    if (aline.empty()) continue;

    std::vector<std::string> line_v = SplitString(aline, ' ');
    assert(line_v.size() > 1);
    assert(line_v[1] == ":");

    int vertex_id = AddVertex(line_v[0], vertex_map, graph);
    graph[vertex_id].is_target = true;
 
    for (int i = 2; i < line_v.size(); i++) {
      int id = AddVertex(line_v[i], vertex_map, graph); 
      graph[vertex_id].neighbors.push_back(id);
    }

    std::getline(myfile, aline); 
  }
}

// Reverses all edges of the given graph.
void ReverseGraph(const std::vector<Vertex>& graph,
                  std::vector<Vertex>& graph_reverse) {
  graph_reverse.resize(graph.size());
  
  for (int i = 0; i < graph.size(); i++) {
    graph_reverse[i].name = graph[i].name;

    for (int j = 0; j < graph[i].neighbors.size(); j++) {
      int index = graph[i].neighbors[j];
      graph_reverse[index].neighbors.push_back(i);
    }
  }
}

// Returns true if the topological sort succeeds, false otherwise.
bool TopologicalSort(const std::vector<Vertex>& graph,
                     std::vector<int>& list) {
  std::vector<Vertex> graph_reverse;
  ReverseGraph(graph, graph_reverse);

  std::vector<int> in_degree;
  for (int i = 0; i < graph.size(); i++) {
    in_degree.push_back(graph[i].neighbors.size());
  }

  std::set<int> remained_vertices;
  for (int i = 0; i < graph.size(); i++) {
    remained_vertices.insert(i);
  }

  std::set<int> deleted_vertices;

  while (!remained_vertices.empty()) {
    int v = -1;  // the vertex whose in_degree is zero.
    for (std::set<int>::const_iterator it = remained_vertices.begin();
         it != remained_vertices.end();
         it++) {
      if (in_degree[*it] == 0) {
        list.push_back(*it);
        v = *it;
        break;
      }
    }

    if (v == -1) {
      return false;
    }
 
    remained_vertices.erase(v);
    deleted_vertices.insert(v);
      
    for (int i = 0; i < graph_reverse[v].neighbors.size(); i++) {
      int neighbor = graph_reverse[v].neighbors[i];
      in_degree[neighbor]--;
    }
  }
  return true;
}

int DepthFirstSearchVisit(const std::vector<Vertex>& graph, 
                          std::set<int>& visited,
                          int index) {
  if (visited.find(index) != visited.end()) {
    return index;
  }

  visited.insert(index);
  for (int i = 0; i < graph[index].neighbors.size(); i++) {
    int circular =
        DepthFirstSearchVisit(graph, visited, graph[index].neighbors[i]);
    if (circular >= 0) {
      return circular;
    }
  }
  return -1;
}

int DepthFirstSearch(const std::vector<Vertex>& graph) {
  std::set<int> visited;
  for (int i = 0; i < graph.size(); i++) {
    int circular = DepthFirstSearchVisit(graph, visited, i);
    if (circular >= 0)
      return circular;
  }
  return -1;
}

int main(int argc, char** argv) {
  if (argc < 2) {
    std::cout << "Usage: " << argv[0] << " inputfile" << std::endl;
    return false;
  }

  std::vector<Vertex> graph;

  BuildGraphFromFile(argv[1], graph);

  std::vector<int> list;

  if (TopologicalSort(graph, list)) {
    PrintSortedList(list, graph);
  } else {
    int circular = DepthFirstSearch(graph);
    assert(circular != -1);
    std::cout << "Error: circular dependency for "
              << graph[circular].name
              << std::endl;
  }

  return true;
}
