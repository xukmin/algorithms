// This program checks if a topological sort result is correct.
//
// Created By: Min Xu <mxu@scu.edu> or <xukmin@gmail.com>
// Date: 11/23/2014

#include <cassert>
#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <vector>

std::vector<std::string> SplitString(std::string source, char splitter) {
  std::vector<std::string> str;
  std::size_t start_index = 0;
  std::size_t found = source.find(splitter);

  while (found != std::string::npos) {
    std::size_t length = found - start_index;
    if (length != 0) {
      str.push_back(source.substr(start_index, length));
    }
    start_index = found + 1;
    found = source.find(splitter, found + 1);
  }

  if (start_index != source.size()) {
    str.push_back(source.substr(start_index));
  }

  return str;
}

bool ReadTopologicalList(char* output_file, 
                         std::map<std::string, int>& sorted_map) {
  std::ifstream myfile(output_file);
  std::string aline;

  std::getline(myfile, aline);
  while (!myfile.eof()) {
    if (aline.empty()) continue;

    std::vector<std::string> line_v = SplitString(aline, ' ');
    if (line_v.size() != 2) {
      std::cerr << "Error: output Format should be : vertex_name check|build"
                << std::endl;
      return false;
    }

    if (sorted_map.find(line_v[0]) != sorted_map.end()) {
      std::cerr << "Error: " << line_v[0] << " is sorted more than once!"
                << std::endl;
      return false;
    }

    int vertex_id = sorted_map.size();
    sorted_map[line_v[0]] = vertex_id;

    std::getline(myfile, aline);
  }

  return true;
}

bool IsTopologicalList(char* input_file,
                       const std::map<std::string, int>& sorted_map) {
  std::ifstream myfile(input_file);
  std::string aline;

  std::getline(myfile, aline);
  while (!myfile.eof()) {
    if (aline.empty()) continue;

    std::vector<std::string> line_v = SplitString(aline, ' ');
    assert(line_v.size() > 1);
    assert(line_v[1] == ":");

    std::vector<int> vertex_id;
    for (int i = 0; i < line_v.size(); i++) {
      if (i == 1) continue;

      std::map<std::string, int>::const_iterator it =
          sorted_map.find(line_v[i]); 
      if (it == sorted_map.end()) {
        std::cerr << "Error: " << line_v[i] 
                  << " is not in the topological list."
                  << std::endl;
        return false;
      }

      vertex_id.push_back(it->second);
    }

    for (int i = 1; i < vertex_id.size(); i++) {
      if (vertex_id[i] >= vertex_id[0]) {
        std::cerr << "Error: " << line_v[i + 1] << " should be at front of "
                  << line_v[0] << "."
                  << std::endl;
        return false;
      }
    }   

    std::getline(myfile, aline);
  }

  return true;
}

int main(int argc, char** argv) {
  if (argc != 3) {
    std::cout << "Usage: " << argv[0] << " <input-file> <output-file>"
              << std::endl;
    return false;
  }

  std::map<std::string, int> sorted_map;
  if (!ReadTopologicalList(argv[2], sorted_map)) {
    return false;
  }

  if (!IsTopologicalList(argv[1], sorted_map)) {
    return false;
  }

  std::cout << "Congratulations, the result is correct! " << std::endl;
  return true;
}
