// This program is to calculate cost of breaking a string.
//
// Created by : Min Xu <mxu@scu.edu> or <xukmin@gmail.com>
// Date : 11/06/2014
 
#include <algorithm>
#include <cassert>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>

#include "BestStringSplitter.h"

void PrintVector(const std::vector<int>& a) {
  if (a.empty()) return;

  for (int i = 0; i + 1 < a.size(); i++) {
    std::cout << a[i] << " ";
  }
  std::cout << a.back();
}

// Adds one split point to the existing sorted split sequence.
// Returns the index.
int InsertSplitPoint(std::vector<int>& splitSeq, int aSplit) {
  splitSeq.push_back(aSplit);

  int j = splitSeq.size() - 2;
  while (j >= 0 && aSplit < splitSeq[j]) {
    std::swap(splitSeq[j], splitSeq[j + 1]);
    j--;
  }  

  return j + 1;
}

int Cost(int stringLength, const std::vector<int>& breakSeq) {
  // Add split points one by one.
  // Initialize with lower bound and upper bound.
  std::vector<int> splitSeq;
  splitSeq.push_back(0);
  splitSeq.push_back(stringLength);

  int cost = 0;
  for (int i = 0; i < breakSeq.size(); i++) {
    assert(breakSeq[i] > 0);
    assert(breakSeq[i] < stringLength);
    int index = InsertSplitPoint(splitSeq, breakSeq[i]);
    cost += splitSeq[index + 1] - splitSeq[index - 1];
  }
  return cost;
}

// The running time is O(nlogn) for sorting, and O(n) for comparisons.
// Overall, it is O(nlogn) + O(n) = O(nlogn)
// We do need a copy of the vector here.
bool HasDuplicates(std::vector<int> array) {
  std::sort(array.begin(), array.end());

  for (int i = 0; i + 1 < array.size(); i++) {
    if (array[i] == array[i + 1]) return true;
  }

  return false;
}

void CalculateCost(int stringLength, const std::vector<int>& breakSeq) {
  int cost = Cost(stringLength, breakSeq);
  std::cout << "Cost: " << cost << std::endl;
}

int factorial(int n) {
  return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}

void PrintBreakSequenceAndCost(const std::vector<int>& breakSeq, int cost) {
  std::cout << "order = ";
  PrintVector(breakSeq);
  std::cout << ", cost = " << cost << std::endl;
}

// Generates the next permutation by lexicographic order.
// See http://en.wikipedia.org/wiki/Permutation
// 1. Find the largest index k such that a[k] < a[k + 1].
// 2. Find the largest index l greater than k such that a[k] < a[l].
// 3. Swap the value of a[k] with that of a[l].
// 4. Reverse the sequence from a[k + 1] up to and 
//    including the final element a[n].
void NextPermutation(std::vector<int>& v) {
  int k = 0;
  for (int i = v.size() - 2; i >= 0; i--) {
    if (v[i] < v[i + 1]) {
      k = i;
      break;
    }
  }

  int l = 0;
  for (int i = v.size() - 1; i > k; i--) {
    if (v[k] < v[i]) {
      l = i;
      break;
    }
  }

  std::swap(v[k], v[l]);

  for (int i = k + 1, j = v.size() - 1; i < j; i++, j--) {
    std::swap(v[i], v[j]);
  }
}

void CostsOfAllPermutations(int stringLength, std::vector<int> array) {
  std::sort(array.begin(), array.end());

  PrintBreakSequenceAndCost(array, Cost(stringLength, array));

  for (int n = 1; n < factorial(array.size()); n++) {
    NextPermutation(array);
    PrintBreakSequenceAndCost(array, Cost(stringLength, array));
  }
}

void BestBreakString(const std::vector<int>& breakSeq) {
  BestStringSplitter bss(breakSeq);
  
  bss.CalculateBestCost();

  int cost = bss.BestCost();
  bss.BestSplitOrder(0, breakSeq.size() - 1);

  std::cout << "Overall: ";
  bss.PrintBestSplitOrder();
  std::cout << ", cost = " << cost << std::endl;
 
  // bss.PrintCacheMatrix();
  for (int i = 1; i + 1 < breakSeq.size(); i++) {
    std::cout << "Choose " << breakSeq[i] << " : " << std::endl;

    if (i == 1) {
      std::cout << "subproblem [] - cost = 0" << std::endl;
    } else {
      std::cout << "subproblem [" << breakSeq[1] << ".." 
                << breakSeq[i - 1] << "] - cost = "
                << bss.BestCost(0, i) << "  ";
      bss.BestSplitOrder(0, i);
      bss.PrintBestSplitOrder();
      std::cout << std::endl;
    } 

    if (i == breakSeq.size() - 2) {
      std::cout << "subproblem [] - cost = 0" << std::endl;
    } else {
      std::cout <<  "subproblem [" << breakSeq[i + 1] << ".." 
                << breakSeq[breakSeq.size() - 2] << "] - cost = " 
                << bss.BestCost(i, breakSeq.size() - 1) << "  ";
      bss.BestSplitOrder(i, breakSeq.size() - 1);
      bss.PrintBestSplitOrder();
      std::cout << std::endl;
    }
  }
}

bool IsInputValid(int stringLength, std::vector<int> breakSeq) {
  if (HasDuplicates(breakSeq)) {
    std::cerr << "ERROR: The sequence of breaks must NOT "
              << "contain any duplicates."
              << std::endl;
    return false;
  }

  for (int i = 0; i < breakSeq.size(); i++) {
    if (breakSeq[i] <= 0 || breakSeq[i] >= stringLength) {
      std::cerr << "ERROR: None of the breaks may occur outside of "
                << "the limits of the string." << std::endl;
      std::cerr << std::endl;
      return false;
    }
  }

  return true;
}

int main(int argc, char** argv) {
  if (argc < 3) {
    std::cout << "Usage: " << argv[0]
              << " cost|all|best <string-length> [split-positions...]"
              << std::endl;
    return false;
  }

  int stringLength = std::atoi(argv[2]);
  std::vector<int> splits;
  for (int i = 3; i < argc; i++) {
    splits.push_back(std::atoi(argv[i]));
    if (splits.back() == 0) {
      std::cerr << "Illegal split position: " << argv[i] << std::endl;
      return false;
    }
  }
  if (!IsInputValid(stringLength, splits)) return false;

  if (std::strcmp(argv[1], "cost") == 0) {
    CalculateCost(stringLength, splits);
  } else if (std::strcmp(argv[1], "all") == 0) {
    CostsOfAllPermutations(stringLength, splits);
  } else if (std::strcmp(argv[1], "best") == 0) {
    splits.push_back(0);
    splits.push_back(stringLength);
    std::sort(splits.begin(), splits.end());
    BestBreakString(splits);
  } else {
    std::cerr << "ERROR: Unknown argument: " << argv[1] << std::endl;
    return false;
  }

  return true;
}
