#include "BestStringSplitter.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <limits>
#include <vector>

BestStringSplitter::BestStringSplitter(const std::vector<int>& splitSeq) 
    : splitSeq_(splitSeq) {
  // Check if the input splitSeq is valid.
  assert(splitSeq_.front() == 0);
  for (int i = 0; i + 1 < splitSeq_.size(); i++) {
    assert(splitSeq_[i] < splitSeq_[i + 1]);
  }

  cacheBestCost.resize(splitSeq_.size());
  cacheFirstPoint.resize(splitSeq_.size());

  for (int i = 0; i < splitSeq_.size(); i++) {
    cacheBestCost[i].resize(splitSeq_.size());
    cacheFirstPoint[i].resize(splitSeq_.size());
  }

  hasCalculatedCost = false;
  hasCalculatedOrder = false;
}

void BestStringSplitter::CalculateBestCost() { 
  RecursiveBestCost(0, splitSeq_.size() - 1);
  hasCalculatedCost = true;
}

int BestStringSplitter::RecursiveBestCost(int splitStartIndex, 
                                          int splitEndIndex) {
  // check cache first
  int costInCache = cacheBestCost[splitStartIndex][splitEndIndex];
  if (costInCache != 0) return costInCache;

  // base case
  if ((splitEndIndex - splitStartIndex) == 1) return 0;

  int myCost = splitSeq_[splitEndIndex] - splitSeq_[splitStartIndex];
  int best = std::numeric_limits<int>::max(); 
  int bestSplitIndex = 0;

  for (int i = splitStartIndex + 1; i < splitEndIndex; i++) {
    int work = RecursiveBestCost(splitStartIndex, i) +
               RecursiveBestCost(i, splitEndIndex);
    if (work < best) {
      best = work;
      bestSplitIndex = i;
    }
  }
  assert(bestSplitIndex != 0);
  best += myCost;

  cacheBestCost[splitStartIndex][splitEndIndex] = best;
  cacheFirstPoint[splitStartIndex][splitEndIndex] = bestSplitIndex;

  return best;
}

int BestStringSplitter::BestCost(int splitStartIndex, int splitEndIndex) {
  if (!hasCalculatedCost) {
    std::cerr << "Call CalculateBestCost() before print!" << std::endl;
    return 0;
  }
  
  return cacheBestCost[splitStartIndex][splitEndIndex];
}

void BestStringSplitter::PrintBestSplitOrder() {
  if (!hasCalculatedCost) {
    std::cerr << "Call CalculateBestCost() before print!" << std::endl;
    return;
  }

  if (!hasCalculatedOrder) {
    std::cerr << "Call BestSplitOrder() before print!" << std::endl;
    return;
  }

  std::cout << "order = ";
  for (int i = 0; i + 1 < bestSplitOrder_.size(); i++) {
    std::cout << bestSplitOrder_[i] << " ";
  }
  std::cout << bestSplitOrder_.back();
}

void BestStringSplitter::PrintCacheMatrix() {
  if (!hasCalculatedCost) {
    std::cerr << "Call BestCost() before print!" << std::endl;
    return;
  }

  for (int i = 0; i < splitSeq_.size(); i++) {
    for (int j = 0; j < splitSeq_.size(); j++) {
      std::cout << std::setw(5) << cacheBestCost[i][j];
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

void BestStringSplitter::BestSplitOrder(int splitStartIndex,
                                        int splitEndIndex) {
  bestSplitOrder_.clear();
  RecursiveBestSplitOrder(splitStartIndex, splitEndIndex);
}

void BestStringSplitter::RecursiveBestSplitOrder(int splitStartIndex,
                                                 int splitEndIndex) {
  if (splitEndIndex - splitStartIndex == 1) return;

  int index = cacheFirstPoint[splitStartIndex][splitEndIndex];
  bestSplitOrder_.push_back(splitSeq_[index]);

  RecursiveBestSplitOrder(splitStartIndex, index);
  RecursiveBestSplitOrder(index, splitEndIndex);
  
  hasCalculatedOrder = true;
}
