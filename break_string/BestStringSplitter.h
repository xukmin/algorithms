// class BestStringSplitter uses dynamic programming to find the optimal order
// to break a string.
//
// Created by : Min Xu <mxu@scu.edu> or <xukmin@gmail.com>
// Date : 11/06/2014

#include <vector>

class BestStringSplitter {
 public:
  // The splitSeq is sorted split positions of a string.
  // Note that splitSeq.front() and splitSeq.back() are not really split
  // positions. splitSeq.front() must be 0, and splitSeq.back() is the length
  // of the string.
  explicit BestStringSplitter(const std::vector<int>& splitSeq);

  void CalculateBestCost();
  void BestSplitOrder(int splitStartIndex, int splitEndIndex);

  void PrintCacheMatrix();
  void PrintBestSplitOrder();

  int BestCost(int splitStartIndex, int splitEndIndex);

  int BestCost() {
    return BestCost(0, splitSeq_.size() - 1);
  }

 private:
  std::vector<int> splitSeq_;  // sorted split positions
  std::vector<int> bestSplitOrder_;  // the order of split points

  std::vector<std::vector<int> > cacheBestCost;
  std::vector<std::vector<int> > cacheFirstPoint;

  bool hasCalculatedCost;  // whether BestCost() is called
  bool hasCalculatedOrder;  // whether BestSplitOrder() is called

  int RecursiveBestCost(int splitStartIndex, int splitEndIndex);
  void RecursiveBestSplitOrder(int splitStartIndex, int splitEndIndex);
};
