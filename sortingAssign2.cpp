// Compare the performance of QuickSort and QuickSort3 
// on arrays of length 5,000,000 for different values of uniqueCount.
//
// Here is the output:
//             Quicksort  Quicksort3
//   5000000,    1.20446,   1.7845
//    500000,    1.17593,  1.58089
//     50000,    1.57805,  1.27676
//      5000,     6.9815, 0.995619
//       500,    63.1978, 0.677158
//        50,    627.999, 0.386192
//
// Created By: Min Xu
// Date: 10/25/2014

#include <cstdio>
#include <ctime>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <time.h>

static int INSERTION_SORT_THRESHOLD;

void Swap(int& a, int& b) {
  int temp = a; 
  a = b; 
  b = temp;
}

void InsertionSort(int* array, int firstIndex, int lastIndex) {
  for (int i = firstIndex + 1; i <= lastIndex; i++) {
    int key = array[i];
  
    int j = i - 1;
    while (j >= firstIndex && key < array[j]) { 
      array[j + 1] = array[j];
      j--;
    }
  
    array[j + 1] = key;
  }
}

// for quick sort in 2 partitions
int Partition2(int* array, int firstIndex, int lastIndex) {
  int pivot = array[firstIndex];

  int i = firstIndex;
  for (int j = firstIndex + 1; j <= lastIndex; j++) {
    if (array[j] < pivot) {
      i++;
      Swap(array[i], array[j]);
    }
  }
  Swap(array[i], array[firstIndex]);

  return i;
}

void QuickSort2(int* array, int firstIndex, int lastIndex) {
  if (lastIndex - firstIndex + 1 < INSERTION_SORT_THRESHOLD) {
    InsertionSort(array, firstIndex, lastIndex);
  } else if (firstIndex < lastIndex) {
    int pIndex = Partition2(array, firstIndex, lastIndex);
    QuickSort2(array, firstIndex, pIndex - 1);
    QuickSort2(array, pIndex + 1, lastIndex);
  }
}

// for quick sort in 3 partitions
std::pair<int, int> Partition3(int* array, int firstIndex, int lastIndex) {
  int pivot = array[firstIndex];

  int i = firstIndex;  // for elements smaller than pivot
  int k = firstIndex;  // for elements equal to pivot

  for (int j = firstIndex + 1; j <= lastIndex; j++) {
    if (array[j] == pivot) {
      k++;
      Swap(array[k], array[j]);
    } else if (array[j] < pivot) {
      k++;
      Swap(array[k], array[j]);
      i++;
      Swap(array[i], array[k]);
    }
  }

  Swap(array[i], array[firstIndex]);

  std::pair<int, int> pIndex;
  pIndex.first = i;
  pIndex.second = k + 1;

  return pIndex;
}

void QuickSort3(int* array, int firstIndex, int lastIndex) {
  if (firstIndex < lastIndex) {
    std::pair<int, int> pIndex = Partition3(array, firstIndex, lastIndex);
    QuickSort3(array, firstIndex, pIndex.first - 1);
    QuickSort3(array, pIndex.second, lastIndex);
  }
}

void GenerateRandomNumbers(int* inputArray, int length) {
  srand(time(NULL));
  
  for (int i = 0; i < length; i++) {
    inputArray[i] = rand();
  }
}

void GenerateUniqueCountNumber(int* inputArray, int length, int uniqueCount) {
  srand(time(NULL));

  for (int i = 0; i < length; i++) {
    inputArray[i] = rand() % uniqueCount;
  }
}

void Print(int* a, int length) {
  std::cout << "Array is :" << std::endl;

  for (int i = 0; i < length; i++) {
    std::cout << a[i] << ", ";
    if ((i + 1) % 5 == 0) std::cout << std::endl;
  }
  std::cout << std::endl;
}

class Timer {
 public:
  Timer() : start_(0), stop_(0) {
  }

  void start() {
    start_ = std::clock();
  }

  void stop() {
    stop_ = std::clock();
  }
  
  double time() {
    return (stop_ - start_) / (double) CLOCKS_PER_SEC;
  }

 private:
  std::clock_t start_;
  std::clock_t stop_;  
};

void CompareInsertionSortAndQuicksort() {
  const int randLength = 5000000;  // 5 * 10^6
  int* randArray = new int[randLength];

  // Save the output for plot the diagrams
  std::ofstream myfile("InsertionSortVsQuickSort.txt");

  // Test subarray lengths from 5 to 300, skipping by 5 each time
  int startSubLengths = 5;
  int endSubLengths = 300;
  int skipLength = 5;

  for (int length = startSubLengths;
       length <= endSubLengths;
       length = length + skipLength) {

    INSERTION_SORT_THRESHOLD = 0;
    GenerateRandomNumbers(randArray, randLength);

    Timer t;
    t.start();
    for (int i = 0; i < randLength - length + 1; i = i + length) {
      InsertionSort(randArray, i, i + length - 1);
    }
    t.stop();
    double timeIS = t.time();

    GenerateRandomNumbers(randArray, randLength);

    t.start();
    for (int i = 0; i < randLength - length + 1; i = i + length) {
      QuickSort2(randArray, i, i + length - 1);
    }
    t.stop();
    double timeQ = t.time();

    myfile << length << " " << timeIS << " " << timeQ << "\n";
  }

  delete[] randArray;
}

void TestInsertionSortThreshold() {
  const int randLength = 4000000;  // 5e6
  int* randArray = new int[randLength];

  // INSERTION_SORT_THRESHOLD from 1 to 200
  std::ofstream myfileTh("Threshold.txt");

  for (int i = 1; i <= 200; i++) {
    INSERTION_SORT_THRESHOLD = i;
    GenerateRandomNumbers(randArray, randLength);

    Timer t;
    t.start();
    QuickSort2(randArray, 0, randLength - 1);
    t.stop();
    double time = t.time();

    myfileTh << i << " " << time << "\n";
  }

  delete[] randArray;
}

void CompareQuicksortAndQuicksort3() {
  // Compare the performance of QuickSort and QuickSort3 
  const int uLength = 5000000;  // 5e6
  int* uArray = new int[uLength];

  for (int uniqueCount = uLength; uniqueCount >= 10; uniqueCount /= 10) {
    INSERTION_SORT_THRESHOLD = 0;
    GenerateUniqueCountNumber(uArray, uLength, uniqueCount);

    Timer t;
    t.start();
    QuickSort2(uArray, 0, uLength - 1);
    t.stop();
    double time2 = t.time();

    GenerateUniqueCountNumber(uArray, uLength, uniqueCount);

    t.start();
    QuickSort3(uArray, 0, uLength - 1);
    t.stop();
    double time3 = t.time();

    std::cout << std::setw(10) << uniqueCount << ", "
              << std::setw(10) << time2 << ", "
              << std::setw(8) << time3
              << std::endl;
  }

  delete[] uArray;
}

int main() {
  CompareInsertionSortAndQuicksort();

  TestInsertionSortThreshold();

  CompareQuicksortAndQuicksort3();

  return true;
}
