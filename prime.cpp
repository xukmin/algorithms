// Four methods to check if a number is a prime number.
//
// Created by: Min Xu
// Date: 10/14/2014
//
//In the range between 1000000 and 1001000
//      Method    # of primes   Max.#operations   Avg.#operations
//   IsPrime_1             75           1000997             74984
//   IsPrime_2             75            500498           37491.7
//   IsPrime_3             75               499           48.3457
//   IsPrime_4             75               168           18.3696
//
//In the range between 10000000 and 10001000
//      Method    # of primes   Max.#operations   Avg.#operations
//   IsPrime_1             61          10000991            609489
//   IsPrime_2             61           5000495            304744
//   IsPrime_3             61              1580           127.866
//   IsPrime_4             61               446            38.987
//
//In the range between 100000000 and 100001000
//      Method    # of primes   Max.#operations   Avg.#operations
//   IsPrime_1             54         100000967       5.39476e+06
//   IsPrime_2             54          50000483       2.69738e+06
//   IsPrime_3             54              4999            335.37
//   IsPrime_4             54              1229           86.5964
//
//In the range between 1000000000 and 1000001000
//      Method    # of primes   Max.#operations   Avg.#operations
//   IsPrime_1             49        1000000991       4.89514e+07
//   IsPrime_2             49         500000495       2.44757e+07
//   IsPrime_3             49             15810           933.661
//   IsPrime_4             49              3401           207.871

#include <cmath>
#include <iostream>
#include <iomanip>
#include <vector>
#include <utility>
#include <stdlib.h>

// for print in column format
static const int methodWidth = 12;
static const int numPrimeWidth = 15;
static const int maxWidth = 18;
static const int avgWidth = 18;

// IsPrime_1 function :
// big-O running time is (num - 2), ie, O(num)
// big-omega running time is (num)
// worst-case : input numbers is a prime
// best-case : input numbers is 2
bool IsPrime_1(int num, int& count) {
  if (num <= 1) return false;

  for(int i = 2; i < num; i++) {
    count++;
    if (num % i == 0) return false;
  } 
  
  return true;
}

// IsPrime_2 function:
// big-O running time is (num - 3) / 2, ie, O(num)
bool IsPrime_2(int num, int& count) {
  if (num <= 1) return false;
  if (num == 2) return true;  // 2 is prime
  if (num % 2 == 0) return false;  // 2 is the only even prime
  
  for (int i = 3; i < num; i = i + 2) {
    count++;
    if (num % i == 0) return false;
  }

  return true;
}

// IsPrime_3 function:
// big-O running time is (sqrt(num) - 3) / 2
// ie, O(sqrt(num))
bool IsPrime_3(int num, int& count) {
  if (num <= 1) return false;
  if (num == 2) return true;  // 2 is prime
  if (num % 2 == 0) return false;  // 2 is the only even prime

  int limit = static_cast<int>(sqrt(num));
  for (int i = 3; i <= limit; i = i + 2) {
    count++;
    if (num % i == 0) return false;
  }

  return true;
}

// IsPrime_4 function:
// big-O running time is the length of primeArray
// ie, O(Pn), Pn = the length of prime array
bool IsPrime_4(int num, const std::vector<int>& primeArray, int& count) {
  if (num <= 1) return false;

  if (primeArray.empty()) {
    std::cerr << "The input of prime array is NOT valid!" << std::endl;
    exit(1);
  }

  if (primeArray.back() < sqrt(num)) {
    std::cerr << "The number of primes are NOT enough to "
              << "test the input value." << std::endl;
    exit(1);
  }

  for (int i = 0; primeArray[i] <= sqrt(num); i++) {
    count++;
    if (num % primeArray[i] == 0) return false;
  }

  return true;
}

class IsPrime_4_Functor {
 public:
  IsPrime_4_Functor(const std::vector<int>& primes) : primes_(primes) {}

  IsPrime_4_Functor(const IsPrime_4_Functor& other) : primes_(other.primes_) {
  }

  bool operator()(int num, int& count) {
    return IsPrime_4(num, primes_, count);
  }

 private:
  const std::vector<int>& primes_;
};

// Find primes using IsPrime_3 function
// in a range between 1 and upperBound (exclusive)
void FindPrimes(int upperBound, std::vector<int>& primes) {
  primes.clear();

  for (int i = 1; i < upperBound; i++) {
    int count = 0;
    if (IsPrime_3(i, count)) {
      primes.push_back(i);
    }
  }
}

// Find the maximum value in a vector
// big-O(n), n = size of the input vector
// if the vector is empty, return -1
int MaxValue(const std::vector<int>& source) {
  int size = source.size();

  if (size == 0) {
    return -1;
  }

  int max = source[0];
  for (int i = 1; i < size; i++) {
    if (max < source[i]) {
      max = source[i];
    }
  } 

  return max;
}

// Calculate the average value of a vector
// big-O(n), n = size of the input vector
// if the vector is empty, return -1
double AverageValue(const std::vector<int>& source) {
  int size = source.size();

  if (size == 0) {
    return -1;
  }

  double total = 0;
  for (int i = 0; i < size; i++) {
    total = total + source[i];
  }

  return total / size;
}

// Count the number of primes during the input range
template <typename IsPrime>
int CountPrimes(IsPrime is_prime,
                int lowerBoundInput,
                int upperBoundInput,
                std::vector<int>& num_ops) {
  int num_primes = 0;
  int size = upperBoundInput - lowerBoundInput + 1;
  num_ops.clear();
  num_ops.resize(size);
  for (int i = lowerBoundInput; i <= upperBoundInput; i++) {
    if (is_prime(i, num_ops[i - lowerBoundInput]))
    {
      num_primes++;
    }
  }

  return num_primes;
}

void Print(std::string funcName,
           int num_primes, 
           const std::vector<int>& num_ops) {
  std::cout << std::setw(methodWidth) << funcName
            << std::setw(numPrimeWidth) << num_primes
            << std::setw(maxWidth) << MaxValue(num_ops)
            << std::setw(avgWidth) << AverageValue(num_ops) << std::endl;
}

int main() {
  // for IsPrime_4 function
  // calculate the prime array
  // the first 6542 primes are enough to test any number up to 2^32
  int upperBound = 7 * 10000;
  std::vector<int> primeArray;
  FindPrimes(upperBound, primeArray);  

  std::vector<std::pair<int, int> > ranges(4);
  ranges[0] = std::make_pair(1000000, 1001000);  // 10^6 ~ 10^6 + 1000
  ranges[1] = std::make_pair(10000000, 10001000);  // 10^7 ~ 10^7 + 1000
  ranges[2] = std::make_pair(100000000, 100001000);  // 10^8 ~ 10^8 + 1000
  ranges[3] = std::make_pair(1000000000, 1000001000);  // 10^9 ~ 10^9 + 1000

  for (int k = 0; k < 4; k++)
  { 
    std::vector<int> num_ops_1;  // number of modulo operations in IsPrime_1
    std::vector<int> num_ops_2;
    std::vector<int> num_ops_3;
    std::vector<int> num_ops_4;

    int num_primes_1 = CountPrimes(&IsPrime_1, 
                                   ranges[k].first, 
                                   ranges[k].second, 
                                   num_ops_1);

    int num_primes_2 = CountPrimes(&IsPrime_2, 
                                   ranges[k].first, 
                                   ranges[k].second, 
                                   num_ops_2);

    int num_primes_3 = CountPrimes(&IsPrime_3,
                                   ranges[k].first, 
                                   ranges[k].second, 
                                   num_ops_3);

    int num_primes_4 = CountPrimes(IsPrime_4_Functor(primeArray),
                                   ranges[k].first, 
                                   ranges[k].second, 
                                   num_ops_4);


    // print the result
    std::cout << "In the range between " << ranges[k].first
              << " and " << ranges[k].second << std::endl;

    std::cout << std::setw(methodWidth) << "Method"
              << std::setw(numPrimeWidth) << "# of primes"
              << std::setw(maxWidth) << "Max.#operations"
              << std::setw(avgWidth) << "Avg.#operations" << std::endl;
 
    Print("IsPrime_1", num_primes_1, num_ops_1);
    Print("IsPrime_2", num_primes_2, num_ops_2);
    Print("IsPrime_3", num_primes_3, num_ops_3);
    Print("IsPrime_4", num_primes_4, num_ops_4);

    std::cout << std::endl;
  }

  return true;
}
